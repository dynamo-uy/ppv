var languageSet = {
  "es-ES": {
    valSociedad: 'NO APLICA',
    palabraClave: 'Documentación Requerida',
    solicitud: 'Por favor adjuntar la siguiente información: (1) Prueba de Origen de Fondos, según declarado anteriormente.',
    nextButtonEnviar: 'Enviar',
    nextButtonGuardar: 'Guardar',
    errorMessage: 'Su formulario no puede ser enviado porque no cumple con alguna de las siguientes condiciones: <br/>',
    condicion1: '- Haber cargado como mínimo 1 Accionista, 1 Director (si la cuenta es de Panama 3) y 1 Beneficiario Final (si la sociedad posee al menos 1 integrante de tipo persona física). <br/>',
    condicion2: '"- La suma de los porcentajes de participación de los accionistas cargados debe ser igual a 100 (cien).',
  },
  "en-US": {
    valSociedad: 'DOESN\'T APPLY',
    palabraClave: 'Requested Documentation',
    solicitud: 'Please upload a copy of the following documentation: (1) Proof of origin of Funds, as stated before.',
    nextButtonEnviar: 'Send',
    nextButtonGuardar: 'Save',
    errorMessage: "Your form can not be sent because it does not meet any of the following conditions: <br/> ",
    condicion1: "- Have loaded at least 1 Shareholder, 1 Director (if the account is from Panama 3) and 1 Final Beneficiary (if the company has at least 1 member of type natural entity). <br/>",
    condicion2: "- The sum of the participation percentages of the loaded shareholders must be equal to 100 (one hundred).",
  }
};

var language = languageSet[$('html').attr('lang')];

$(document).ready(function() {
  $("#dyn_formularioenviado").change(ChangeLabelButton);
  SolicitaAdjuntos();
  OcultarCampos();
});

$(document).ready(function() { 
	visibilidad();
	deshabilitar();
	OcultarCampos();
  $("#dyn_nombrecuentaalternativo2").change(visibilidad);
  $("#dyn_formularioenviado").change(ChangeLabelButton);
});

function OcultarCampos() {
    let campoBusinessLine = $(`fieldset:contains("Business line")`);
    if (campoBusinessLine.length) {
      campoBusinessLine.hide();
    }                                   
    let campoBusinessLineSP = $(`fieldset:contains("Area de Servicios")`);
    if (campoBusinessLineSP.length) {
      campoBusinessLineSP.hide();
    }
    $('h2[class="tab-title"]').hide();
    if(location.pathname.indexOf('AperturaCuentaServiciosAProspects') >= 0 ||
        location.pathname.indexOf('AperturaCuentaServiciosBProspects') >= 0) {  
        let campoPleasePress = $(`fieldset:contains("Please press Submit button to end your request.")`);
        if (campoPleasePress.length) {
            campoPleasePress.show();
        }
        let campoPleasePressLegend = $(`legend:contains("Please press Submit button to end your request.")`);
        if (campoPleasePressLegend.length) {
            campoPleasePressLegend.attr('style', 'font-size:15px');
        }
    }
    else {
        let campoPleasePress = $(`fieldset:contains("Please press Submit button to end your request.")`);
        if (campoPleasePress.length) {
            campoPleasePress.hide();
        }
    }
}

function visibilidad() {
	var valSociedad = $("#dyn_nombrecuentaalternativo2").length
    	? $("#dyn_nombrecuentaalternativo2").val().toUpperCase()
    	: '';
  
	if(valSociedad === language.valSociedad)
	{
		ShowOrHide("#yominame", false);
		ShowOrHide("#dyn_nombrecuentaalternativo2", false);
		ShowOrHide("#dyn_nombrecuentaalternativo3", false);
		ShowOrHide("#dyn_nombrecuentaalternativo4", false);
	}
	else
	{
	  ShowOrHide("#dyn_nombrecuenta_name", false);
	}

}

function ShowOrHide(campo, visible){
	if(visible)
    $(campo).closest("tr").show();
   else
    $(campo).closest("tr").hide();
}

function deshabilitar() {
	var valTitulosSocietarios = $("#dyn_destinottulosaccionarios").length
    	? $("#dyn_destinottulosaccionarios").val().toUpperCase()
    	: '';
  
	if(valTitulosSocietarios !== "")
	{
	  $("#dyn_destinottulosaccionarios").prop( 'disabled', true );
	}
}

function SolicitaAdjuntos() {
    if(!$('label').length) {
        console.error('la etiqueta label no se encontró');
        return;
    }
    var max = $('label').contents().length;
    var i;
    for (i = 0; i < max; i++) { 
    if($('label').contents()[i].textContent === language.palabraClave){
      $('label').contents()[i].textContent = language.solicitud;
      break;
    }
  }
}

function ChangeLabelButton() {
	var fld = $("#dyn_formularioenviado_1").is(":checked");
	if(fld !== undefined && fld === true ) 
		$("#NextButton").attr('value', language.nextButtonEnviar);
	else
		$("#NextButton").attr('value', language.nextButtonGuardar);
}

document.addEventListener('DOMContentLoaded', function() {
    if(document.getElementById('dyn_origen') instanceof HTMLElement)
        document.getElementById('dyn_origen').parentNode.parentNode.parentNode.style.display = 'none';
});

$('.text-danger').html('<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>&nbsp'+language.errorMessage + language.condicion1 + language.condicion2);