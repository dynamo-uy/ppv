$('#NextButton').on('mousedown', function(event) {
    var languageSet = {
        "es-ES": {
            erroresTitle: 'Errores:',
    		erroresTexto: 'Debe completar todos los campos'
        },
        "en-US": {
            erroresTitle: 'Errors:',
    		erroresTexto: 'You must complete all the fields'
        }
    };
	
	var language  = languageSet[$('html').attr('lang')]; 
	
  	var error = false;
	
	if($(this).attr('onclick')) {
		window.script = $(this).attr('onclick').split(':')[1];
		$(this).removeAttr('onclick');
	}
	var errorTitle = document.createElement('h4');
	errorTitle.setAttribute('class', 'alert-heading');
	errorTitle.innerText = language.erroresTitle;
	$('select:visible').each(function(i) {
	  if(!this.value)
			error = true;
	});
	if(error){
		$('.alert').css('display', 'none');
		var alert = document.createElement('div');
		alert.setAttribute('class', 'alert alert-danger');
		alert.setAttribute('role', 'alert');
		var p = document.createElement('p');
		p.innerText = language.erroresTexto;
		alert.appendChild(p);
		document.getElementById('liquid_form').prepend(alert);
		window.location.href = '#liquid_form';
	} else {
		$(this).attr('onclick', window.script);
	}
});