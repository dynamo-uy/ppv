var languageSet = {
    "es-ES": {
        valSociedad: 'NO APLICA',
        palabraClave: 'Documentación requerida',
        solicitud:'Por favor adjuntar la siguiente información: (1) Prueba de Origen de Fondos, según declarado anteriormente.',
        nextButtonGuardar: 'Guardar',
        nextButtonEnviar: 'Enviar'
    },
    "en-US": {
        valSociedad: 'DOESN\'T APPLY',
        palabraClave: 'Requested Information',
        solicitud: 'Please upload a copy of the following documentation: (1) Funds Source proof, as stated before.',
        nextButtonGuardar: 'Save',
        nextButtonEnviar: 'Send'
    }
};

var language  = languageSet[$('html').attr('lang')];

$(document).ready(function() {
  SolicitaAdjuntos();
	visibilidad();
	deshabilitar();
  $("#dyn_nombrecuentaalternativo2").change(visibilidad);
  $("#dyn_formularioenviado").change(ChangeLabelButton);
});

function visibilidad() {
	var valSociedad = $("#dyn_nombrecuentaalternativo2").val().toUpperCase();
  
	if(valSociedad === language.valSociedad)
	{
		ShowOrHide("#yominame", false);
		ShowOrHide("#dyn_nombrecuentaalternativo2", false);
		ShowOrHide("#dyn_nombrecuentaalternativo3", false);
		ShowOrHide("#dyn_nombrecuentaalternativo4", false);
	}
	else
	{
	  ShowOrHide("#dyn_nombrecuenta_name", false);
	}

}

function ShowOrHide(campo, visible){
	if(visible)
    $(campo).closest("tr").show();
   else
    $(campo).closest("tr").hide();
}

function deshabilitar() {
	var valTitulosSocietarios = $("#dyn_destinottulosaccionarios").val().toUpperCase();
  
	if(valTitulosSocietarios !== "")
	{
	  $("#dyn_destinottulosaccionarios").prop( 'disabled', true );
	}
}

function SolicitaAdjuntos() {
  var palabraClave = language.palabraClave;
  var solicitud  = language.solicitud;
  var max = $('label').contents().length;
  var i;
  for (i = 0; i < max; i++) { 
    if($('label').contents()[i].textContent === palabraClave){
      $('label').contents()[i].textContent = solicitud;
      break;
    }
  }
}

function ChangeLabelButton() {
	var fld = $("#dyn_formularioenviado_1").is(":checked");
	if(fld !== undefined && fld === true )
		$("#NextButton").attr('value', language.nextButtonEnviar);
	else
		$("#NextButton").attr('value', language.nextButtonGuardar);
}
