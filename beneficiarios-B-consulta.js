const lang = $('html').attr('lang');
const langSet = {
    es: {
        personType: 'PERSONA FÍSICA',
    },
    en:  {
        personType: 'INDIVIDUAL',
    },
};
window.languageSet = langSet[lang.split('-')[0]];

$(document).ready(function() {
 $('#dyn_director_0').on('click', function() {
   $('#dyn_cargo').val('');
 });
 $('#dyn_accionista_0').on('click', function() {
   $('#dyn_participacion').val('');
 });
 $('#dyn_apoderado_0').on('click', function() {
   $('#dyn_facultades').val('');
 });
 $('#dyn_jointtenancy_0').on('click', function() {
   $('#dyn_participacionjointtenancy').val('');
 });
 
 $('#dyn_personalidad').on('mousedown', function() { return false; });
 $('[data-name="tab_6"] input').prop('readonly', true);
  $.getScript('https://brainclientaccessnew.microsoftcrmportals.com/dynamo-cdn', function() {
    console.log('Extensiones de Dynamo cargadas correctamente');
  });
  
  /*$.getScript('https://brainclientaccessnew.microsoftcrmportals.com/form-helper.js', function() {
    $('.form-helper').css({
      'color': '#3cbed3',
      'cursor': 'pointer'
    });
  });*/
  
  SetFieldsVisibility();
  $("#dyn_director").change(SetFieldsVisibility);
  $("#dyn_accionista").change(SetFieldsVisibility);
  $("#dyn_apoderado").change(SetFieldsVisibility);
 $("#dyn_jointtenancy").change(SetFieldsVisibility);  
  $("#dyn_personalidad").change(SetFieldsVisibility);

});

function SetFieldsVisibility() {
 ShowOrHide("#dyn_director_1", "#dyn_cargo");
 ShowOrHide("#dyn_accionista_1", "#dyn_participacion");
 ShowOrHide("#dyn_apoderado_1", "#dyn_facultades");
 ShowOrHide("#dyn_jointtenancy_1", "#dyn_participacionjointtenancy");
 ShowOrHideKind(); 
}

function ShowOrHide(origen, destino){
 var fld = $(origen).is(":checked");
 if (fld != undefined && fld == true )
    $(destino).closest("tr").show();
  else
    $(destino).closest("tr").hide();
}

function ShowOrHideKind(){
  var valKind = $("#dyn_personalidad option:selected").text().toUpperCase();

 if(valKind === window.languageSet.personType)
 {
   $("#dyn_nombresociedad").closest("tr").hide(); 
    $("#dyn_beneficiariofinal").closest("tr").show();
  $("#dyn_nombrealterno").closest("tr").show();
  $("#dyn_apellido").closest("tr").show();
  $("#dyn_correoelectronico").closest("tr").show();
   $("#dyn_beneficiariofinalnombre").closest("tr").hide();
  $("#dyn_beneficiariofinalapellido").closest("tr").hide();
  $("#dyn_beneficiariofinalmail").closest("tr").hide();
  $(".tab-title")[3].innerText = "";
 }
 else
 {
   $("#dyn_nombresociedad").closest("tr").show();
   $("#dyn_beneficiariofinal").closest("tr").hide();
  $("#dyn_nombrealterno").closest("tr").hide();
  $("#dyn_apellido").closest("tr").hide();
  $("#dyn_correoelectronico").closest("tr").hide();
   $("#dyn_beneficiariofinalnombre").closest("tr").show();
  $("#dyn_beneficiariofinalapellido").closest("tr").show();
  $("#dyn_beneficiariofinalmail").closest("tr").show();
  $(".tab-title")[3].innerText = "Beneficiario final";
 }
}