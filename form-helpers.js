var fhlangSet = {
    "es-ES": {
        ayudaNoEncontrada: 'Ayuda no encontrada',
        ayudaNoDisponible: 'Ayuda no disponibleen construcción',
    },
    "en-US": {
        ayudaNoEncontrada: 'Form helper not found',
        ayudaNoDisponible: 'Form helper not available',
    }
};
var fhlang = fhlangSet[document.querySelector('html').lang];

function openModal(body) {
var modal0 = "<div id=\"form-helper\" class=\"modal fade\" role=\"dialog\" style=\"display: none\">" +
		"<div class=\"modal-dialog\">" +
			"<!-- Modal content-->" +
			"<div class=\"modal-content\">" +
				"<div class=\"modal-header\">" +
					"<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>" +
				"</div>" +
				"<div class=\"modal-body\">" +
					body +
				"</div>" +
			"</div>" +
		"</div>" +
	"</div>";

	$(modal0).modal('show');
}

function getFormHelper(header, helper, apiURL) {
	var input = helper.parent().next().children(),
		inputGroup = input.first('.input-group').children(),
		id = null,
		label = helper.closest("label"),
		lang = $('html').attr('lang').split('-')[0];

	if(input.length > 1 && !!inputGroup.eq(2).attr('id')) {
		id = inputGroup.eq(2).attr('id');
	} else if(!!label.attr('for')) {
		id = label.attr('for');
	} else if(!!input.attr('id'))
		id = input.attr('id');
	
	if(id && id.indexOf('_entityname') > 0) 
		id = id.substr(0, id.indexOf('_entityname'));
	else if(id && id.indexOf('_description') > 0)
		id = id.substr(0, id.indexOf('_description'));
	else if(id && id.indexOf('_label') > 0)
		id = id.substr(0, id.indexOf('_label'));
	else if($(helper).parent().next().is('[data-composite-control]'))
	    id = $(helper).parent().next().attr('id');
		
	if(!id) {
		console.error('Ayuda no encontrada');
		openModal(fhlang.ayudaNoEncontrada);
		return;
	}
		
	var params = '';
		
	if(header.text().length)
		params += '?title=' + header.text().trim();
	else {
		var args = location.search.substr(1, location.search.length);
		if(args.indexOf('entityformid') > 0) {
			args = args.substr(args.indexOf('entityformid'), args.length);
			var parameters = args.split(/&/);
			parameters = parameters[0];
			var entityformid = parameters.split(/=/)[1];
		} else
			var entityformid = args.split('=')[1];
		params += '?entityform=' + entityformid;
	}
	if (params.length) {
		params += '&fieldname=' + id;
	}

	if (params.length) {
		params += `&lang=${lang}`;
	} else {
		params = `?lang=${lang}`;
	}
	
	console.log('Ayuda del Portal:', params);
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', apiURL + params, true);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4) {
		   if(xhr.status == 200) {
				var jsonResponse = JSON.parse(this.response),
					result = jsonResponse.results[0];
					if(result && result[lang])
						openModal(result[lang]);
					else {
						console.error('Ayuda no disponible');
						openModal('<h1>' + fhlang.ayudaNoDisponible + '</h1>');
					}
		   } else {
			console.error('Error en carga de API ayuda formularios');
		   }
		}
	};
	xhr.send(null);
}

/* NOTAS:
 * Para incluir en el metadato: <span class="form-helper"><i class="fa fa-question-circle"></i></span>
 */
