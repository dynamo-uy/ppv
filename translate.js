$.fn.extend({
  /* La función acepta dos parámetros: un lenguage de entrada (dos caracteres), y uno de salida (dos caracteres)*/
  translate: async function(sourceLang, targetLang) {
    /*
     * Tener en cuenta:
     * - El uso de $ en variables locales refiere al ámbito
     * en el que se encuentra, siendo <$ambito> un ejemplo.
     * 
     * Mejoras:
     * - Iteración de objetos
     *
     * Restricciones:
     * - Texto vacío
     * - Números
     * - Idioma base = Idioma objetivo
     */
	  if($(this).text().trim().length < 1 || !isNaN(parseInt($(this).text().trim())) || sourceLang == targetLang)
      return;
    
	  let $function = this;
	  let coleccion = [];
	  let traduccion = '';
	  let elementos = [];

	  $(this).each(async function() {
		  if ($(this).children().length) {
			  elementos = $(this).children();
		  } else {
			  elementos = $(this);
      }
      
      elementos.each(async function() {
        const elemento = $(this);
  
     $.ajax({
          url: 'https://translate.yandex.net/api/v1.5/tr.json/translate',
          method: 'GET',
          async: true,
          dataType: 'JSONP',
          crossDomain: true,
          data: {
            key: 'trnsl.1.1.20181205T124404Z.59a0c0f166494b77.5fab0174f9ccbc0d648a66f8918b7548c1aca8ff',
            text: elemento.text(),
            lang: sourceLang + '-' + targetLang
          },
          success: async function(response) {
            try {
              if(response.code !== 200) {
                throw "Respuesta: " + response.code;
              }
              elemento.text(response.text);
            } catch (e) {
               throw "Respuesta: " + e.message;
            }
          },
          error: function(xhr, status, error) {
            console.error('Mensaje devuelto por el servidor:', xhr.responseText);
          }
        })
      })
    });
  }
});
