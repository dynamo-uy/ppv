const BRAINURL = location.origin;
var languageSet = {
    "es-ES": {
        errorTitle: "Errores",
        physicalPerson: "Persona física",
        legalPerson: "Persona jurídica",
        nombreAlterno: 'El nombre no puede estar vacio',
        apellido: 'El apellido no puede estar vacio',
        correoElectronicoEmpty: 'El correo electronico no puede estar vacio',
        correoElectronicoError: 'El correo electronico no es valido',
        nombreSociedad: 'El nombre de la sociedad no puede estar vacio',
        beneficiarioFinalNombre: 'El nombre del beneficiario final no puede estar vacio',
        beneficiarioFinalApellido: 'El apellido del beneficiario final no puede estar vacio',
        beneficiarioFinalEmailEmpty: 'El correo electronico del beneficiario final no puede estar vacio',
        beneficiarioFinalEmailError: 'El correo electronico del beneficiario final no es valido',
        naturalezaIntegrante: 'Debe indicar la naturaleza del intgrante',
        physicalPersonTitle: 'PERSONA FÍSICA',
        beneficiarioFinalTitle: 'Beneficiario final',
        extensiones: 'Extensiones de Dynamo cargadas correctamente'
    },
    "en-US": {
        errorTitle: "Errors",
        physicalPerson: "Individual",
        legalPerson: "Legal person",
        nombreAlterno: 'Name is mandatory',
        apellido: 'Surname is mandatory',
        correoElectronicoEmpty: 'The e-mail address is mandatory',
        correoElectronicoError: 'The e-mail address is not valid',
        nombreSociedad: 'The society\'s name is mandatory',
        beneficiarioFinalNombre: 'The final beneficiary\'s name is mandatory',
        beneficiarioFinalApellido: 'The final beneficiary\'s surname is mandatory',
        beneficiarioFinalEmailEmpty: 'The final beneficiary\'s e-mail address is mandatory',
        beneficiarioFinalEmailError: 'The final beneficiary\'s e-mail address is not valid',
        naturalezaIntegrante: 'You must indicate member\'s nature',
        physicalPersonTitle: 'INDIVIDUAL',
        beneficiarioFinalTitle: 'Final Beneficiary',
        extensiones: 'Dynamo extensions loaded successfully'
    }
};

var language = languageSet[$('html').attr('lang')];

$(document).ready(function() {
    $('#dyn_director_0').on('click', function() {
        $('#dyn_cargo').val('');
    });
    $('#dyn_accionista_0').on('click', function() {
        $('#dyn_participacion').val('');
    });
    $('#dyn_apoderado_0').on('click', function() {
        $('#dyn_facultades').val('');
    });
    $('#dyn_jointtenancy_0').on('click', function() {
        $('#dyn_participacionjointtenancy').val('');
    });

	$('#InsertButton').on('mousedown', function(event) {
        var errors = false; 
        if($(this).attr('onclick')) {
            window.script = $(this).attr('onclick').split(':')[1];
            $(this).removeAttr('onclick');
        }
        var errorList = document.createElement('ul'),
            errorTitle = document.createElement('h4');  
        errorTitle.setAttribute('class', 'alert-heading');
        errorTitle.innerText = `${language['errorTitle']}:`;
        errorList.appendChild(errorTitle);
        switch($('#dyn_personalidad option:selected').text()) {
            case language['physicalPerson']:
                if($('#dyn_nombrealterno').val().trim().length < 1) {
                    var li = document.createElement('li');
                    li.innerText = language['nombreAlterno'];
                    errorList.appendChild(li);
                    errors = true;
                }
                if($('#dyn_apellido').val().trim().length < 1) {
                    var li = document.createElement('li');
                    li.innerText = language['apellido'];
                    errorList.appendChild(li);
                    errors = true;
                }
                if($('#dyn_correoelectronico').val().trim().length < 1) {
                    var li = document.createElement('li');
                    li.innerText = language['correoElectronicoEmpty'];
                    errorList.appendChild(li);
                    errors = true;
                } else if(!validateEmail($('#dyn_correoelectronico').val())) {
                    var li = document.createElement('li');
                    li.innerText = language['correoElectronicoError'];
                    errorList.appendChild(li);
                    errors = true;
                }
                break;
            case language['legalPerson']:
                if($('#dyn_nombresociedad').val().trim().length < 1) {
                    var li = document.createElement('li');
                    li.innerText = language['nombreSociedad'];
                    errorList.appendChild(li);
                    errors = true;
                }
                if($('#dyn_beneficiariofinalnombre').val().trim().length < 1) {
                    var li = document.createElement('li');
                    li.innerText = language['beneficiarioFinalNombre'];
                    errorList.appendChild(li);
                    errors = true;
                }
                if($('#dyn_beneficiariofinalapellido').val().trim().length < 1) {
                    var li = document.createElement('li');
                    li.innerText = language['beneficiarioFinalApellido'];
                    errorList.appendChild(li);
                    errors = true;
                }
                if($('#dyn_beneficiariofinalmail').val().trim().length < 1) {
                    var li = document.createElement('li');
                    li.innerText = language['beneficiarioFinalEmailEmpty'];
                    errorList.appendChild(li);
                    errors = true;
                } else if(!validateEmail($('#dyn_beneficiariofinalmail').val())) {
                    var li = document.createElement('li');
                    li.innerText = language['beneficiarioFinalEmailError'];
                    errorList.appendChild(li);
                    errors = true;
                }
                break;
            default:
                var li = document.createElement('li');
                    li.innerText = language['naturalezaIntegrante'];
                    errorList.appendChild(li);
                break;
        }
        if(errors) {
            $('.alert').css('display', 'none');
            var alert = document.createElement('div');
            alert.setAttribute('class', 'alert alert-danger');
            alert.setAttribute('role', 'alert');
            alert.appendChild(errorList);
            document.getElementById('content_form').prepend(alert);
            window.location.href = '#content_form';
        } else {
            $(this).attr('onclick', window.script);
        }
    });
    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
  
  $.getScript(BRAINURL + '/dynamo-cdn', function() {
    console.log('Extensiones de Dynamo cargadas correctamente');
  });

  SetFieldsVisibility();
  $("#dyn_director").change(SetFieldsVisibility);
  $("#dyn_accionista").change(SetFieldsVisibility);
  $("#dyn_apoderado").change(SetFieldsVisibility);
  $("#dyn_personalidad").change(SetFieldsVisibility);

});

function SetFieldsVisibility() {
	ShowOrHide("#dyn_director_1", "#dyn_cargo");
	ShowOrHide("#dyn_accionista_1", "#dyn_participacion");
	ShowOrHide("#dyn_apoderado_1", "#dyn_facultades");
	ShowOrHideKind();	
}

function ShowOrHide(origen, destino){
	var fld = $(origen).is(":checked");
	if (fld != undefined && fld == true )
    $(destino).closest("tr").show();
  else
    $(destino).closest("tr").hide();
}

function ShowOrHideKind(){
  var valKind = $("#dyn_personalidad option:selected").text().toUpperCase();
	
	if(valKind === language['physicalPersonTitle'])
	{
	  $("#dyn_nombresociedad").closest("tr").hide();	
    $("#dyn_beneficiariofinal").closest("tr").show();
		$("#dyn_nombrealterno").closest("tr").show();
		$("#dyn_apellido").closest("tr").show();
		$("#dyn_correoelectronico").closest("tr").show();
	  $("#dyn_beneficiariofinalnombre").closest("tr").hide();
		$("#dyn_beneficiariofinalapellido").closest("tr").hide();
		$("#dyn_beneficiariofinalmail").closest("tr").hide();
		$(".tab-title")[3].innerText = "";
	}
	else
	{
	  $("#dyn_nombresociedad").closest("tr").show();
	  $("#dyn_beneficiariofinal").closest("tr").hide();
		$("#dyn_nombrealterno").closest("tr").hide();
		$("#dyn_apellido").closest("tr").hide();
		$("#dyn_correoelectronico").closest("tr").hide();
	  $("#dyn_beneficiariofinalnombre").closest("tr").show();
		$("#dyn_beneficiariofinalapellido").closest("tr").show();
		$("#dyn_beneficiariofinalmail").closest("tr").show();
		$(".tab-title")[3].innerText = language.beneficiarioFinalTitle;
	}
}