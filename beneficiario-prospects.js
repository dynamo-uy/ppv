const BRAINURL = location.origin;
var languageSet = {
    "es-ES": {
        barraProgreso: 'Apertura Cuenta de Servicios',
        erroresTitle: 'Errores:',
		erroresTexto: 'Debe completar todos los campos'
    },
    "en-US": {
		barraProgreso: 'Open Services Account',
        erroresTitle: 'Errors:',
		erroresTexto: 'You must complete all the fields'
    }
};

var language  = languageSet[$('html').attr('lang')]; 

$(document).ready(function() {
  visibilidad();
  $('select').on('change',function(ev){ doChange(this.id)});  
});

function doChange(controlId)
{
	var hayDatos = false	

	$('select').each(function () {
		if(($(this).attr("id") == controlId))
		{
			//debugger;
			switch (controlId) {
				case "dyn_areaservicios":
					hayDatos = changeAreaServicios();
					if(!hayDatos)
						controlId = "dyn_tiposervicio";
                    //sete el campo tipo de servicio multiple selección y toda la logica para poder seleccionar varias líneas	
                    multiSelectCtrl()
					break;
				case "dyn_tiposervicio":
					hayDatos = changeTipoServicio();
					if(!hayDatos)
						controlId = "dyn_jurisdiccionsociedad";
					break;
				case "dyn_jurisdiccionsociedad":
					hayDatos = changeJurisdiccion();
					if(!hayDatos)
						controlId = "dyn_legalform";
					break;
				case "dyn_legalform":
					hayDatos = changeLegalForm();
					if(!hayDatos)
						controlId = "dyn_fundlicense";
					break;
				case "dyn_fundlicense":
					hayDatos = changeFundLicense();
					if(!hayDatos)
						controlId = "dyn_tipoincorporacion";
					break;
				case "dyn_tipoincorporacion":
					hayDatos = changeTipoIncorporacion();
					if(!hayDatos)
						controlId = "dyn_integraciondirectoriosociedad";
					break;
				case "dyn_integraciondirectoriosociedad":
					hayDatos = changeIntegracionDirectorio();
					if(!hayDatos)
						controlId = "dyn_estructuracapital";
					break;
				case "dyn_estructuracapital":
					hayDatos = changeEstructura();
					if(!hayDatos)
						controlId = "dyn_jointtenancywithrightfirstsurvivorship";
					break;
			}

			if(fieldToChange(controlId))
			{
				ShowOrHide('#' + controlId, hayDatos && fieldToChange(controlId));
			}
		}
	});
}

function fieldToChange(field)
{
	var result = false;
	
	switch (field) {
		case "dyn_areaservicios":
		case "dyn_tiposervicio":
		case "dyn_jurisdiccionsociedad":
		case "dyn_legalform":
		case "dyn_fundlicense":
		case "dyn_tipoincorporacion":
		case "dyn_integraciondirectoriosociedad":
		case "dyn_estructuracapital":
		case "dyn_jointtenancywithrightfirstsurvivorship":
			result = true;
			break;
	}
	
	return result;		
}

//Manejan eventos de OnChange.
function changeAreaServicios(){
	$('#dyn_tiposervicio, #dyn_jurisdiccionsociedad, #dyn_legalform, #dyn_fundlicense, #dyn_tipoincorporacion, #dyn_integraciondirectoriosociedad, #dyn_estructuracapital').value = '';

	var filtro = getFilterAreaServicio();
	var hayDatos = false;
	
	if(filtro !== '')
	{
		hayDatos = executeQuery(filtro, "#dyn_tipodeservicios");
	}
	
	return hayDatos;
}

function changeTipoServicio(){
	var filtro = getFilterAreaServicio();
	var filtroTipoServicio = getFilterTipoServicio();
	var hayDatos = false;
	
	if(filtroTipoServicio !== '')
	{
		filtro += " and " + filtroTipoServicio;
	}
	
	hayDatos = executeQuery(filtro, "#dyn_jurisdiccindelasociedad");
	
	return hayDatos;	
}

function changeJurisdiccion(){
	var filtro = getFilterAreaServicio();
	var filtroTipoServicio = getFilterTipoServicio();
	var filtroJurisdiccion = getFilterJurisdiccion();
	var hayDatos = false;
	
	if(filtroTipoServicio !== '')
	{
		filtro += " and " + filtroTipoServicio;
	}
	
	if(filtroJurisdiccion !== '')
	{
		filtro += " and " + filtroJurisdiccion;
	}
	
	hayDatos = executeQuery(filtro, '#dyn_legalform');	
	
	return hayDatos;	
}

function changeLegalForm(){
	var filtro = getFilterAreaServicio();
	var filtroTipoServicio = getFilterTipoServicio();
	var filtroJurisdiccion = getFilterJurisdiccion();
	var filtroLegalForm = getFilterLegalForm();
	var hayDatos = false;
	
	if(filtroTipoServicio !== '')
	{
		filtro += " and " + filtroTipoServicio;
	}
	
	if(filtroJurisdiccion !== '')
	{
		filtro += " and " + filtroJurisdiccion;
	}
	
	if(filtroLegalForm !== '')
	{
		filtro += " and " + filtroLegalForm;
	}

	hayDatos = executeQuery(filtro, '#dyn_fundlicense');	
	
	return hayDatos;	
}

function changeFundLicense(){
	var filtro = getFilterAreaServicio();
	var filtroTipoServicio = getFilterTipoServicio();
	var filtroJurisdiccion = getFilterJurisdiccion();
	var filtroLegalForm = getFilterLegalForm();
	var filtroFundLicense = getFilterFundLicense();	
	var hayDatos = false;
	
	if(filtroTipoServicio !== '')
	{
		filtro += " and " + filtroTipoServicio;
	}
	
	if(filtroJurisdiccion !== '')
	{
		filtro += " and " + filtroJurisdiccion;
	}
	
	if(filtroLegalForm !== '')
	{
		filtro += " and " + filtroLegalForm;
	}

	if(filtroFundLicense !== '')
	{
		filtro += " and " + filtroFundLicense;
	}

	hayDatos = executeQuery(filtro, '#dyn_tipodeincorporacin');	
	
	return hayDatos;	
}

function changeTipoIncorporacion(){
	var filtro = getFilterAreaServicio();
	var filtroTipoServicio = getFilterTipoServicio();
	var filtroJurisdiccion = getFilterJurisdiccion();
	var filtroLegalForm = getFilterLegalForm();
	var filtroFundLicense = getFilterFundLicense();		
	var filtroTipoIncorporacion = getFilterTipoIncorporacion();
	var hayDatos = false;
	
	if(filtroTipoServicio !== '')
	{
		filtro += " and " + filtroTipoServicio;
	}
	
	if(filtroJurisdiccion !== '')
	{
		filtro += " and " + filtroJurisdiccion;
	}
	
	if(filtroTipoIncorporacion !== '')
	{
		filtro += " and " + filtroTipoIncorporacion;
	}

	if(filtroLegalForm !== '')
	{
		filtro += " and " + filtroLegalForm;
	}

	if(filtroFundLicense !== '')
	{
		filtro += " and " + filtroFundLicense;
	}

	hayDatos = executeQuery(filtro, '#dyn_integracindeldirectoriodelasociedad');	
	
	return hayDatos;	
}

function changeIntegracionDirectorio(){
	var filtro = getFilterAreaServicio();
	var filtroTipoServicio = getFilterTipoServicio();
	var filtroJurisdiccion = getFilterJurisdiccion();
	var filtroLegalForm = getFilterLegalForm();
	var filtroFundLicense = getFilterFundLicense();		
	var filtroTipoIncorporacion = getFilterTipoIncorporacion();
	var filtroIntegracionDirectorio = getFilterIntegracionDirectorio();
	var hayDatos = false;
	
	if(filtroTipoServicio !== '')
	{
		filtro += " and " + filtroTipoServicio;
	}	
	
	if(filtroJurisdiccion !== '')
	{
		filtro += " and " + filtroJurisdiccion;
	}

	if(filtroLegalForm !== '')
	{
		filtro += " and " + filtroLegalForm;
	}

	if(filtroFundLicense !== '')
	{
		filtro += " and " + filtroFundLicense;
	}	
	
	if(filtroTipoIncorporacion !== '')
	{
		filtro += " and " + filtroTipoIncorporacion;
	}	
	
	if(filtroIntegracionDirectorio !== '')
	{
		filtro += " and " + filtroIntegracionDirectorio;
	}

	hayDatos = executeQuery(filtro, '#dyn_estructuradecapitalautorizado');	
	
	return hayDatos;	
}

function changeEstructura(){
	var filtro = getFilterAreaServicio();
	var filtroTipoServicio = getFilterTipoServicio();
	var filtroJurisdiccion = getFilterJurisdiccion();
	var filtroLegalForm = getFilterLegalForm();
	var filtroFundLicense = getFilterFundLicense();		
	var filtroTipoIncorporacion = getFilterTipoIncorporacion();
	var filtroIntegracionDirectorio = getFilterIntegracionDirectorio();
	var filtroEstructuraCapital = getFilterEstructuraCapital();
	var hayDatos = false;
	
	if(filtroTipoServicio !== '')
	{
		filtro += " and " + filtroTipoServicio;
	}
	
	if(filtroJurisdiccion !== '')
	{
		filtro += " and " + filtroJurisdiccion;
	}
	
	if(filtroLegalForm !== '')
	{
		filtro += " and " + filtroLegalForm;
	}

	if(filtroFundLicense !== '')
	{
		filtro += " and " + filtroFundLicense;
	}
	
	if(filtroTipoIncorporacion !== '')
	{
		filtro += " and " + filtroTipoIncorporacion;
	}
	
	if(filtroIntegracionDirectorio !== '')
	{
		filtro += " and " + filtroIntegracionDirectorio;
	}
	
	if(filtroEstructuraCapital !== '')
	{
		filtro += " and " + filtroEstructuraCapital;
	}

	hayDatos = executeQuery(filtro, '#dyn_jointtenancy');	
	
	return hayDatos;
}

function getFilterAreaServicio(){
	var filterOption = '';
	var valorId = $("#dyn_areaservicios").val();
	var areaServicios = $("#dyn_areaservicios :selected").text();	
	
	if(valorId !== ''){
		filterOption = "dyn_readeservicios/Id eq (guid'" + valorId + "')";
		ShowOrHideDivByArea($("#dyn_areaservicios :selected").text());
	}
	
	if(areaServicios==="Fund Services" || areaServicios==="Private Wealth Services")
        $('div[id="ctrl-click"]').show();
    else    
        $('div[id="ctrl-click"]').hide();
	
	return filterOption;
}

function getFilterTipoServicio(){
	var filterOption = '';
	var valorId = $("#dyn_tiposervicio").val();
	var areaServicios = $("#dyn_areaservicios :selected").text();

	if(valorId !== ''){
		if(areaServicios != 'Corporate Services' && areaServicios != 'Business Process Services'){
			$.each(valorId, function( key, value ) {
				if(value != "")
				{
					if(key > 0)
					{
						filterOption += " or ";
					}	
					filterOption += "dyn_tipodeservicios/Id eq (guid'" + value + "')";
				}
			});
			
			if(filterOption != "")
			{
				filterOption = "(" + filterOption + ")";
			}
		}
		else{
			filterOption = "dyn_tipodeservicios/Id eq (guid'" + valorId + "')";
		}
	}
	
	setTypeServicesSelection();
	
	return filterOption;
}

function setTypeServicesSelection()
{
	var selected = '';
	var inc = 0;
	
	$('#dyn_tiposervicio :selected').each(function(){
		selected += $(this).text() + ', ';
    });
		
	$("#dyn_typeservicesselection").val(selected);
}

function getFilterJurisdiccion(){
	var filterOption = '';	
	var valorId = $("#dyn_jurisdiccionsociedad").val();
	
	if( valorId !== ''){
		filterOption = "dyn_jurisdiccindelasociedad/Id eq (guid'" + valorId + "')";		
	}
	
	return filterOption;
}

function getFilterLegalForm(){
	var filterOption = '';	
	var valorId = $("#dyn_legalform").val();
	
	if( valorId !==''){
		filterOption = "dyn_legalform/Value eq " + valorId;		
	}
	
	return filterOption;
}

function getFilterFundLicense(){
	var filterOption = '';	
	var valorId = $("#dyn_fundlicense").val();
	
	if( valorId !==''){
		filterOption = "dyn_fundlicense/Value eq " + valorId;		
	}
	
	return filterOption;
}

function getFilterTipoIncorporacion(){
	var filterOption = '';	
	var valorId = $("#dyn_tipoincorporacion").val();
	
	if( valorId !==''){
		filterOption = "dyn_tipodeincorporacin/Value eq " + valorId;		
	}
	
	return filterOption;
}

function getFilterIntegracionDirectorio(){
	var filterOption = '';	
	var valorId = $("#dyn_integraciondirectoriosociedad").val();
	$("#dyn_integraciondirectoriosociedad").prop("selected","selected");	
	
	if( valorId !== ''){
		filterOption = "dyn_integracindeldirectoriodelasociedad/Value eq " + valorId;		
	}
	
	return filterOption;
}

function getFilterEstructuraCapital(){
	var filterOption = '';	
	var valorId = $("#dyn_estructuracapital").val();
		$("#dyn_estructuracapital").prop("selected","selected");
	
	if( valorId !== ''){
		filterOption = "dyn_estructuradecapitalautorizado/Id eq (guid'" + valorId + "')";		
	}
	
	return filterOption;
}

function visibilidad() {
		
		ShowOrHide("#dyn_tiposervicio", false);
		ShowOrHide("#dyn_typeservicesselection", false);
		ShowOrHide("#dyn_jurisdiccionsociedad", false);
		ShowOrHide("#dyn_legalform", false);
		ShowOrHide("#dyn_fundlicense", false);
		ShowOrHide("#dyn_integraciondirectoriosociedad", false);
		ShowOrHide("#dyn_estructuracapital", false);
		ShowOrHide("#dyn_tipoincorporacion", false);
		ShowOrHide("#dyn_jointtenancywithrightfirstsurvivorship", false);
		ShowOrHideDivByArea("HideAll");
		
		$('.progress').append($('<li class="list-group-item incomplete">' + language.barraProgreso +'</li>'))
}

function getCampoSiguiente(campoActual){
  var campoSiguiente = '';

  switch (campoActual.toLowerCase()) {
  		case "#dyn_areaservicio":
  			campoSiguiente = "#dyn_tiposervicio";
  			break;
  		case "#dyn_tiposervicio":
  			campoSiguiente = "#dyn_jurisdiccindelasociedad";
  			break;
  		case "#dyn_jurisdiccindelasociedad":
  			campoSiguiente = "#dyn_legalform";
  			break;
  		case "#dyn_legalform":
  			campoSiguiente = "#dyn_fundlicense";
  			break;
  		case "#dyn_fundlicense":
  			campoSiguiente = "#dyn_tipodeincorporacin";
  			break;
  		case "#dyn_tipodeincorporacin":
  			campoSiguiente = "#dyn_integracindeldirectoriodelasociedad";
  			break;
  		case "#dyn_integracindeldirectoriodelasociedad":
  			campoSiguiente = "#dyn_estructuradecapitalautorizado";
  			break;
		case "#dyn_estructuradecapitalautorizado":
  			campoSiguiente = "#dyn_jointtenancy";
  			break;
  		default:
  			campoSiguiente = '';
  			break;
  }
  
  return campoSiguiente;
}

function ejecutarCampoSiguiente(campoSiguiente){
  
  switch (campoSiguiente.toLowerCase()) {
  		case "#dyn_areaservicios":
  			changeAreaServicios();
  			break;
  		case "#dyn_tiposervicio":
  			changeTipoServicio();
  			break;
  		case "#dyn_jurisdiccindelasociedad":
  			changeJurisdiccion();
  			break;
  		case "#dyn_legalform":
  			changeTipoServicio();
  			break;
  		case "#dyn_fundlicense":
  			changeTipoServicio();
  			break;
  		case "#dyn_tipodeincorporacin":
  			changeTipoIncorporacion();
  			break;
  		case "#dyn_integracindeldirectoriodelasociedad":
  			changeIntegracionDirectorio();
  			break;
		case "#dyn_estructuradecapitalautorizado":
  			changeEstructura()
  			break;
  		default:
  			campoSiguiente = '';
  			break;
			
		ShowOrHide(campoSiguiente, true);
  }
}

function getCampoPortal(nombreCRM){
  var campoPortal = '';

  switch (nombreCRM.toLowerCase()) {
  		case "#dyn_readeservicios":
  			campoPortal = "#dyn_areaservicios";
  			break;	  
  		case "#dyn_tipodeservicios":
  			campoPortal = "#dyn_tiposervicio";
  			break;	  
  		case "#dyn_estructuradecapitalautorizado":
  			campoPortal = "#dyn_estructuracapital";
  			break;
  		case "#dyn_integracindeldirectoriodelasociedad":
  			campoPortal = "#dyn_integraciondirectoriosociedad";
  			break;
  		case "#dyn_jointtenancy":
  			campoPortal = "#dyn_jointtenancywithrightfirstsurvivorship";
  			break;
  		case "#dyn_jurisdiccindelasociedad":
  			campoPortal = "#dyn_jurisdiccionsociedad";
  			break;
  		case "#dyn_legalform":
  			campoPortal = "#dyn_legalform";
  			break;
  		case "#dyn_fundlicense":
  			campoPortal = "#dyn_fundlicense";
  			break;
  		case "#dyn_tipodeincorporacin":
  			campoPortal = "#dyn_tipoincorporacion";
  			break;
  		default:
  			campoPortal = '';
  			break;
  }

  return campoPortal;
}

function getCampoCRM(nombrePortal){
  var campoCRM = '';

  switch (nombreCRM.toLowerCase()) {
  		case "#dyn_areaservicios":
  			campoCRM = "#dyn_readeservicios";
  			break;
  		case "#dyn_tiposervicio":
  			campoCRM = "#dyn_tipodeservicios";
  			break;
  		case "#dyn_estructuracapital":
  			campoCRM = "#dyn_estructuradecapitalautorizado";
  			break;
  		case "#dyn_integraciondirectoriosociedad":
  			campoCRM = "#dyn_integracindeldirectoriodelasociedad";
  			break;
  		case "#dyn_jointtenancywithrightfirstsurvivorship":
  			campoCRM = "#dyn_jointtenancy";
  			break;
  		case "#dyn_jurisdiccionsociedad":
  			campoCRM = "#dyn_jurisdiccindelasociedad";
  			break;
  		case "#dyn_legalform":
  			campoCRM = "#dyn_legalform";
  			break;
  		case "#dyn_fundlicense":
  			campoCRM = "#dyn_fundlicense";
  			break;
  		case "#dyn_tipoincorporacion":
  			campoCRM = "#dyn_tipodeincorporacin";
  			break;
  		default:
  			campoCRM = '';
  			break;
  }
  
  return campoCRM;
}

function limpiarCombo(control){
  $(getCampoPortal(control) + " > option").each(function() {
    if(this.value !== "")
    {
      this.remove();
    }
  });
}

function executeQuery(filteroption, nextField){
    var odataUri = BRAINURL + "/_odata/dyn_condicionesservicioacontratar";
	if(filteroption.length) {
		odataUri += "?$filter=" + encodeURIComponent(filteroption);
	}
    var actValue = "";
    var JointTenancyTrue = 0;
    var JointTenancyFalse = 0;
	var hayDatos = false;

	try {
		limpiarCombo(nextField);
		$.ajax({  
			type: 'GET',  
			contentType: 'application/json; charset=utf-8',  
			datatype: 'json',
			url: odataUri,  
		}).done(function(json){
			var colCondiciones = json.value;
			$.each(colCondiciones, function(index, condicion){
				switch (nextField.toLowerCase()) {
				case "#dyn_tipodeservicios":
					if(condicion.hasOwnProperty("dyn_tipodeservicios") && condicion.dyn_tipodeservicios !== null 
					&& condicion.dyn_tipodeservicios.Id !== '')
					{
					  if (!valorExistente("#dyn_tiposervicio" , condicion.dyn_tipodeservicios.Name) && condicion.dyn_tipodeservicios.Name != "Do not want to hire a service")
						{
						  $("#dyn_tiposervicio").append('<option value="'+ 
						    condicion.dyn_tipodeservicios.Id + '">' + 
						    condicion.dyn_tipodeservicios.Name + '</option>');
						}
						hayDatos = true;
					}
					break;
				case "#dyn_jurisdiccindelasociedad":
					if(condicion.hasOwnProperty("dyn_jurisdiccindelasociedad") && condicion.dyn_jurisdiccindelasociedad !== null 
					&& condicion.dyn_jurisdiccindelasociedad.Id !== '')
					{	
					  if (!valorExistente("#dyn_jurisdiccionsociedad" , condicion.dyn_jurisdiccindelasociedad.Name))
						{
						  $("#dyn_jurisdiccionsociedad").append('<option value="'+ 
						    condicion.dyn_jurisdiccindelasociedad.Id + '">' + 
						    condicion.dyn_jurisdiccindelasociedad.Name + '</option>');
						}
						hayDatos = true;
					}
					
					break;
				case "#dyn_legalform":
					if(condicion.hasOwnProperty("dyn_legalform") && condicion.dyn_legalform !== null
					&& condicion.dyn_legalform.Value !== null && condicion.dyn_legalform.Value !== '')
					{	
						if (!valorExistente("#dyn_legalform" , condicion.dyn_legalform.Name))					
						{
						  $("#dyn_legalform").append('<option value="'+ 
						    condicion.dyn_legalform.Value + '">' + 
						    condicion.dyn_legalform.Name + '</option>');
						}
						hayDatos = true;
					}
					
					break;
				case "#dyn_fundlicense":
					if(condicion.hasOwnProperty("dyn_fundlicense") && condicion.dyn_fundlicense !== null
					&& condicion.dyn_fundlicense.Value !== null && condicion.dyn_fundlicense.Value !== '')
					{	
						if (!valorExistente("#dyn_fundlicense" , condicion.dyn_fundlicense.Name))					
						{
						  $("#dyn_fundlicense").append('<option value="'+ 
						    condicion.dyn_fundlicense.Value + '">' + 
						    condicion.dyn_fundlicense.Name + '</option>');
						}
						hayDatos = true;
					}
					
					break;
				case "#dyn_tipodeincorporacin":
					if(condicion.hasOwnProperty("dyn_tipodeincorporacin") && condicion.dyn_tipodeincorporacin !== null
					&& condicion.dyn_tipodeincorporacin.Value !== null && condicion.dyn_tipodeincorporacin.Value !== '')
					{	
						if (!valorExistente("#dyn_tipoincorporacion" , condicion.dyn_tipodeincorporacin.Name))					
						{
						  $("#dyn_tipoincorporacion").append('<option value="'+ 
						    condicion.dyn_tipodeincorporacin.Value + '">' + 
						    condicion.dyn_tipodeincorporacin.Name + '</option>');
						}
						hayDatos = true;
					}
					
					break;
				case "#dyn_integracindeldirectoriodelasociedad":
					if(condicion.hasOwnProperty("dyn_integracindeldirectoriodelasociedad") && condicion.dyn_integracindeldirectoriodelasociedad !== null
					&& condicion.dyn_integracindeldirectoriodelasociedad.Value !== null && condicion.dyn_integracindeldirectoriodelasociedad.Value !== '')
					{	
						if (!valorExistente("#dyn_integraciondirectoriosociedad" , condicion.dyn_integracindeldirectoriodelasociedad.Name))
						{
						  $("#dyn_integraciondirectoriosociedad").append('<option value="'+ 
						    condicion.dyn_integracindeldirectoriodelasociedad.Value + '">' + 
						    condicion.dyn_integracindeldirectoriodelasociedad.Name + '</option>');
						}
						hayDatos = true;
					}
					break;
				case "#dyn_estructuradecapitalautorizado":
					if(condicion.hasOwnProperty("dyn_estructuradecapitalautorizado") && condicion.dyn_estructuradecapitalautorizado !== null 
					&& condicion.dyn_estructuradecapitalautorizado.Id !== '')
					{	
						if (!valorExistente("#dyn_estructuracapital" , condicion.dyn_estructuradecapitalautorizado.Name))
						{
						  $("#dyn_estructuracapital").append('<option value="'+ 
						    condicion.dyn_estructuradecapitalautorizado.Id + '">' + 
						    condicion.dyn_estructuradecapitalautorizado.Name + '</option>');
						}
						hayDatos = true;
					}
					break;
				case "#dyn_jointtenancy":
					if(condicion.hasOwnProperty("dyn_jointtenancy") && condicion.dyn_jointtenancy !== null 
					&& condicion.dyn_jointtenancy !== '')
					{
					  if(condicion.dyn_jointtenancy === true)
					  {
					    JointTenancyTrue = JointTenancyTrue + 1;
					  }
					  else
					  {
					    JointTenancyFalse = JointTenancyFalse + 1;
					  }
					  hayDatos = true;
					}
					break;
				default:
				  hayDatos = false;
					break;
				}
					
				hayCampos(hayDatos, nextField, JointTenancyTrue, JointTenancyFalse);
			})
		}); 	
	}
	catch(err){
		console.error(err);
	}
	
	return hayDatos;
}

function hayCampos(hayDatos, nextField, JointTenancyTrue, JointTenancyFalse) {
	if(hayDatos){
	  ShowOrHide(getCampoPortal(nextField), hayDatos);

	  if(nextField === "#dyn_jointtenancy")
	  {
		if((JointTenancyTrue > 0) || (JointTenancyFalse > 0))
		{
			if((JointTenancyTrue > 0) && (JointTenancyFalse === 0))
			{
				ChangeProp("#dyn_jointtenancywithrightfirstsurvivorship_1", 'checked',true);
			}
		  
			if((JointTenancyTrue === 0) && (JointTenancyFalse > 0))
			{
				ChangeProp("#dyn_jointtenancywithrightfirstsurvivorship_0", 'checked',true);
			}

			if((JointTenancyTrue > 0) && (JointTenancyFalse > 0))
			{
				ChangeProp("#dyn_jointtenancywithrightfirstsurvivorship_1", 'disabled', false );
				ChangeProp("#dyn_jointtenancywithrightfirstsurvivorship_0", 'disabled', false );			        
			}
			else
			{
				ChangeProp("#dyn_jointtenancywithrightfirstsurvivorship_1", 'disabled', true );
				ChangeProp("#dyn_jointtenancywithrightfirstsurvivorship_0", 'disabled', true );			        
			}
		 }
		 ChangeProp("#NextButton", 'disabled', false);
	  }
   }
}

function valorExistente(campo, valor)
{
	var retorno = false;
	
	$(campo + "> option").each(function() {
		if(this.text === valor)
		{
			retorno = true;
		}
	});
	
	return retorno;
}

function ChangeProp(campo, atributo, valor)
{
	$(campo).prop(atributo, valor);
}

function ShowOrHide(campo, visible){
	if(visible){
		$(campo).closest("tr").show();
	}
	else{
		$(campo).closest("tr").hide();
	}
}

function ShowOrHideDivByArea(area){
	switch (area) {
		case "Business Process Services":
			$('div[data-name="Others"]').show();
			$('div[data-name="Fund_Services"]').hide();
			$('h2[class="tab-title"]').hide();
			break;  
		case "Private Wealth Services":
			$('div[data-name="Others"]').show();
			$('div[data-name="Fund_Services"]').hide();	
			$('h2[class="tab-title"]').hide();
			break;  
		case "Fund Services":
			$('div[data-name="Fund_Services"]').show();
			$('div[data-name="Others"]').hide();
			$('h2[class="tab-title"]').hide();
			break;  
		default:
			$('div[data-name="Others"]').hide();
			$('div[data-name="Fund_Services"]').hide();
			$('h2[class="tab-title"]').hide();
			break
	}
}

function split(str)
{
	var partialStr = "";
	var character = "";
	var arrayRes = [];
	var numArray = 0;

	if(str.length > 0)
	{
		for (num=0; num <= str.length; num++)
		{
			character = str.substr(num,1);
			
			if((character == ",") || (num + 1 == str.length))
			{
			  if(num + 1 == str.length) partialStr += character;
			  arrayRes[numArray] = partialStr;
			  numArray++;
			  partialStr = "";
			}
			else
			{
			  partialStr += character;
			}
		}
	}
	
	return arrayRes;
}

$('#NextButton').on('mousedown', function(event) {
	
  	var error = false;
	
	if($(this).attr('onclick')) {
		window.script = $(this).attr('onclick').split(':')[1];
		$(this).removeAttr('onclick');
	}
	
	var errorTitle = document.createElement('h4');
	errorTitle.setAttribute('class', 'alert-heading');
	errorTitle.innerText = language.erroresTitle;
	$('select:visible').each(function(i) {
	  if(!this.value)
			error = true;
	    
	});
	
	if(error) {
		$('.alert').css('display', 'none');
		var alert = document.createElement('div');
		alert.setAttribute('class', 'alert alert-danger');
		alert.setAttribute('role', 'alert');
		var p = document.createElement('p');
		p.innerText = language.erroresTexto;
		alert.appendChild(p);
		document.getElementById('liquid_form').prepend(alert);
		window.location.href = '#liquid_form';
	} else {
		$(this).attr('onclick', window.script);
	}
});

function multiSelectCtrl(){
    var area;
    $("#dyn_areaservicios").focusout(function (){
        area=$("#dyn_areaservicios option:selected").text();
        console.log("valor de area"+area)
    })//fin de focusout area

    var repo=[];
    $("#dyn_tiposervicio").focusin(function(){
        if(area==="Fund Services" || area==="Private Wealth Services"){
            //elimino el option que mustra los selected
            $("#dyn_tiposervicio option[value='valor']").remove();
            // seteo multiple
            $(this).attr('multiple','multiple');
        }//fin if
    })
    $("#dyn_tiposervicio").focusout(function (){
        setTypeServicesSelection();
    if(area==="Fund Services" || area==="Private Wealth Services"){
	    var selected = '';
        var inc = 0;
	    $('#dyn_tiposervicio :selected').each(function(){
		selected += $(this).text() + ', ';
                });
		$("#dyn_typeservicesselection").val(selected);
		repo.push(selected);
        //remuevo atributo multiple
        $('#dyn_tiposervicio').removeAttr('multiple','multiple');
        $('#dyn_tiposervicio').append('<option value="valor" selected>'+selected+'</option>');
        $("#dyn_typeservicesselection").val(selected);
        }//fin if
    })//fin focus out tipo de Servicio
}//fin multiselect Datos
