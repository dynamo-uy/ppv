var languageSet = {
  "es-ES": {
    palabraClave: 'Documentación requerida',
    solicitud: 'Por favor adjuntar la siguiente información: (1) Prueba de Origen de Fondos, según declarado anteriormente.',
    nextButtonGuardar: 'Guardar',
    nextButtonEnviar: 'Enviar'
  },
  "en-US": {
    palabraClave: 'Requested Documentation',
    solicitud: 'Please upload a copy of the following documentation: (1) Funds Source proof, as stated before.',
    nextButtonGuardar: 'Save',
    nextButtonEnviar: 'Send'
  }
};
//-------------------------------------------------------------------------
//Christopher 15-11-19
$("#NextButton").click( function (){
 /*ErrorMsg('#yominame');
 ErrorMsg('#industrycode');
 ErrorMsg('#dyn_reseniaactividades');
 ErrorMsg('#dyn_origenvolumensoc');
 ErrorMsg('#dyn_volumendefondosdelasociedadmilesus');
 ErrorMsg('#dyn_reseniaorigenvolumensoc');
*/
 var tabla= div.getElementsByTagName("table");
 console.log(tabla)
}) 
//Fin Christopher
//--------------------------------------------------------------------------
var language = languageSet[$('html').attr('lang')];
$(document).ready(function() {
    $("#dyn_formularioenviado").change(ChangeLabelButton);
    SolicitaAdjuntos();
	visibilidad();
	deshabilitar();
  $("#dyn_nombrecuentaalternativo2").change(visibilidad);
});

function visibilidad() {
    var valSociedad; 
    if($("#dyn_nombrecuentaalternativo2").val()=== undefined){
        console.log("dyn_nombrecuentaalternativo2 undefined");
    }else{
        valSociedad = $("#dyn_nombrecuentaalternativo2").val().toUpperCase();
    }
	if(valSociedad === language.valSociedad)
	{
		ShowOrHide("#yominame", false);
		ShowOrHide("#dyn_nombrecuentaalternativo2", false);
		ShowOrHide("#dyn_nombrecuentaalternativo3", false);
		ShowOrHide("#dyn_nombrecuentaalternativo4", false);
	}
	else
	{
	  ShowOrHide("#dyn_nombrecuenta_name", false);
	}

}
//
function ShowOrHide(campo, visible){
	if(visible)
    $(campo).closest("tr").show();
   else
    $(campo).closest("tr").hide();
}
//
function deshabilitar() {
    var valTitulosSocietarios;
    
    if($("#dyn_destinottulosaccionarios").val()=== undefined){
        console.log("dyn_destinottulosaccionarios undefined")
    }else{
        valTitulosSocietarios = $("#dyn_destinottulosaccionarios").val().toUpperCase();
    }

 
	if(valTitulosSocietarios !== "")
	{
	  $("#dyn_destinottulosaccionarios").prop( 'disabled', true );
	}
}
//
function SolicitaAdjuntos() {
  var max = $('label').contents().length;
  var i;
  for (i = 0; i < max; i++) {
    if($('label').contents()[i].textContent === language.palabraClave){
      $('label').contents()[i].textContent = language.solicitud;
      break;
    }
  }
}
//
function ChangeLabelButton() {
	var fld = $("#dyn_formularioenviado_1").is(":checked");
	if(fld !== undefined && fld === true ){
		$("#NextButton").attr('value', language.nextButtonEnviar);
	}
	else
	    $("#NextButton").attr('value', language.nextButtonGuardar);
	}
$(window).load(function() {
  let consentimiento = '';
  let salvarForm = '';
  
  switch($('html').attr('lang')) {
    case 'en-US':
      cosentimiento = 'Client Consent';
      salvarForm = 'Save / Send Form';
      break;
    case 'es-ES':
      cosentimiento = 'Consentimiento Cliente';
      salvarForm = 'Guardar/Enviar Formulario';
      break;
  }
  $(`legend:contains(${salvarForm})`).parent().show();
  $(`legend:contains(${cosentimiento})`).parent().hide();
});
function ErrorMsg(campo){
	
    if($("#dyn_formularioenviado_1").is(":checked")){
        if($(campo).val().length){
         }else{
        switch (campo) {
            case "#yominame":
            $('#ValidationSummaryEntityFormView ul').append("<li><a href='#yominame_label' onclick='javascript:scrollToAndFocus(&quot;yominame_label&quot;,&quot;yominame&quot;);return false;'>Name of the applicant entity (alt. 1) is a required field. </a></li>")
        break;
            case "#industrycode":
            $('#ValidationSummaryEntityFormView ul').append("<li><a href='#industrycode_label' onclick='javascript:scrollToAndFocus(&quot;industrycode_label&quot;,&quot;industrycode&quot;);return false;'>Type of activities to be developed by the Entity is a required field.</a></li>")
        break;
            case "#dyn_reseniaactividades":
            $('#ValidationSummaryEntityFormView ul').append("<li><a href='#dyn_reseniaactividades_label' onclick='javascript:scrollToAndFocus(&quot;dyn_reseniaactividades_label&quot;,&quot;dyn_reseniaactividades&quot;);return false;'>Description of the activities to be developed by the Company is a required field.</a></li>")
        break;
            case "#dyn_origenvolumensoc":
            $('#ValidationSummaryEntityFormView ul').append("<li><a href='#dyn_origenvolumensoc_label' onclick='javascript:scrollToAndFocus(&quot;dyn_origenvolumensoc_label&quot;,&quot;dyn_origenvolumensoc&quot;);return false;'>Origin of the funds of the intended activities is a required field. </a></li>")
        break;
            case "#dyn_volumendefondosdelasociedadmilesus":
            $('#ValidationSummaryEntityFormView ul').append("<li><a href='#dyn_volumendefondosdelasociedadmilesus_label' onclick='javascript:scrollToAndFocus(&quot;dyn_volumendefondosdelasociedadmilesus_label&quot;,&quot;dyn_volumendefondosdelasociedadmilesus&quot;);return false;'>Volume of the funds of the intended activities (thousand U$S) is a required field.</a></li>")
        break;
            case "#dyn_reseniaorigenvolumensoc":
            $('#ValidationSummaryEntityFormView ul').append("<li><a href='#dyn_reseniaorigenvolumensoc_label' onclick='javascript:scrollToAndFocus(&quot;dyn_reseniaorigenvolumensoc_label&quot;,&quot;dyn_reseniaorigenvolumensoc&quot;);return false;'>Description of origin and volume of funds of the inteded activities is a required field. </a></li>")
        break;
            }
        }
    }
}//
//comprobar contenido en SharedHoders
function shareholderTest(table){
		$(table).map(td => cosole.log(table));
}


