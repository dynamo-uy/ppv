var languageSet = {
  "es-ES": {
    palabraClave: 'Documentación requerida',
    solicitud: 'Por favor adjuntar la siguiente información: (1) Copia de los Estatutos Sociales o Contrato Social; (2) Copia de los Registros Accionistas/Directores (en caso de aplicar); (3)Copia de Certificado de Vigencia (ultimo emitido, en caso de aplicar); (4) Documento que acredite la representación legal del Representante (en caso de aplicar); (5) Copia de los títulos accionarios (de corresponder); (6) Copia del certificado de incorporación (de corresponder); (7) Copia de las resoluciones de la sociedad (de corresponder), incluyendo la designación de los primeros directores; (8) Carta de referencias bancarias y profesionales y/o comerciales (vigencia no superior a 3 meses); (9) Documentos que acrediten el cumplimiento de las obligaciones tributarias con la administración fiscal que corresponda; (10) Prueba de Origen de Fondos, según declarado anteriormente.',
    nextButtonGuardar: 'Guardar',
    nextButtonEnviar: 'Enviar'
  },
  "en-US": {
    palabraClave: 'Requested Documentation',
    solicitud: 'Please upload a copy of the following documentation, if applicable: (1) Company´s bylaws, (2) Share Register and Register of Directors, (3) Certificate of Goodstanding and Incumbency (last ones issued)  (4) Any document proving the legal powers of the Company´s representative, (5) Share certificates, (6) Certificate of incorporation, (7) Company´s resolutions, including the appointment of first directors, 8) Banking and Professional or Commercial references with a validity of no longer than three (3) months, 9) Tax returns in the country of residence of the shareholders and 10) Income source proof.',
    nextButtonGuardar: 'Save',
    nextButtonEnviar: 'Send'
  }
};

var language = languageSet[$('html').attr('lang')];

$(document).ready(function() {
  $("#dyn_formularioenviado").change(ChangeLabelButton);
  SolicitaAdjuntos();
});

function SolicitaAdjuntos() {  
  $('#address1_composite_line2').prev('label').hide();
  $('#address1_composite_line3').prev('label').hide();
  $("#address1_composite_line2").hide();
  $("#address1_composite_line3").hide();
  
  var max = $('label').contents().length;
  var i;
  for (i = 0; i < max; i++) {
    if($('label').contents()[i].textContent === language.palabraClave){
      $('label').contents()[i].textContent = language.solicitud;
      break;
    }
  }
}

//$('#WebFormControl_848b0e40c3a7e811a96b000d3ac1b81c').prepend($('<ol class="progress list-group top"><li class="list-group-item incomplete">Cliente Potencial</li><li class="list-group-item incomplete">Servicio a contratar</li><li class="list-group-item active">Apertura Cuenta de Servicios</li></ol>'));

function ChangeLabelButton() {
	var fld = $("#dyn_formularioenviado_1").is(":checked");
	if(fld !== undefined && fld === true )
		$("#NextButton").attr('value', language.nextButtonEnviar);
	else
		$("#NextButton").attr('value', language.nextButtonGuardar);
}

$(window).load(function() {
  let consentimiento = '';
  let salvarForm = '';
  
  switch($('html').attr('lang')) {
    case 'en-US':
      cosentimiento = 'Client Consent';
      salvarForm = 'Save / Send Form';
      break;
    case 'es-ES':
      cosentimiento = 'Consentimiento Cliente';
      salvarForm = 'Guardar/Enviar Formulario';
      break;
  }
  
  $(`legend:contains(${salvarForm})`).parent().show();
  
  if($(`legend:contains(${cosentimiento})`).length)
    $(`legend:contains(${cosentimiento})`).parent().hide();
});
