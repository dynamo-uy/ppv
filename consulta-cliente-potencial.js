const BRAINURL = location.origin;
$(document).ready(function(){
  var bancoPadre = getUsuerBancoPadre();
  var banco = getUserBanco();

  $(".entitylist.entity-grid").on("loaded", function () {
     $(this).children(".view-grid").find("tr").each(function (){ 
     if(finicio){
       finicio = false;
     }
     else{
      var eliminar = true;
      var elemento = $(this);
      var itemBancoPadre = elemento.find("td[data-attribute='dyn_bancopadre']").data("value") !== undefined ? elemento.find("td[data-attribute='dyn_bancopadre']").data("value").Name : undefined; 
      var itemBanco = elemento.find("td[data-attribute='dyn_bancoasociado']").data("value") !== undefined ? elemento.find("td[data-attribute='dyn_bancoasociado']").data("value").Name : undefined; 

   if(isEmptyOrNull(itemBanco) && isEmptyOrNull(itemBancoPadre))
   {eliminar = true;}
   else if((!isEmptyOrNull(banco))
   && (!isEmptyOrNull(bancoPadre))
   && (!isEmptyOrNull(itemBanco))
   && banco === itemBanco)
   {//Si el creador tiene banco, y tiene banco padre. Solamente banco = bancoAsoaciado.
     eliminar = false;}
   else if((!isEmptyOrNull(banco))
   && isEmptyOrNull(bancoPadre) 
   && (((!isEmptyOrNull(itemBanco)) && banco === itemBanco)
   || ((!isEmptyOrNull(itemBancoPadre)) && banco === itemBancoPadre)))
   {//Si el creador tiene solo banco y no tiene banco padre, solamente banco = padre
     eliminar = false;}
     
   /*else if(bancoPadre !== undefined && bancoPadre !== null && (bancoPadre === itemBanco || bancoPadre === itemBancoPadre)){//comparar banco padre con ambos bancos de la grilla, Si alguno es igual ya se pasa a false
   eliminar = false;}
   else if(banco !== undefined && banco !== null && (banco === itemBanco || banco=== itemBancoPadre)){
   eliminar = false;}*/

      
   if(eliminar === true){
   $(this).closest("tr[data-entity='lead']").hide(); 
  }
     }
    
  //}    
     });
  });

//$(".entitylist.entity-grid").children(".view-grid").find("td[data-attribute='dyn_bancopadre']").hide();

 $(".entitylist.entity-grid").on("loaded", function(){
   $(".entitylist.entity-grid").find("td[data-attribute='dyn_bancopadre']").hide();
   $(".entitylist.entity-grid").find("a[aria-label='Parent Bank']").hide();
   $(".entitylist.entity-grid").find("a[aria-label='Intermediary']").hide();
   $(".entitylist.entity-grid").find("td[data-attribute='dyn_bancoasociado']").hide();
 //var i=0;
  //$('a[href*="' + BRAINURL + '"]').each(function(){
   //$('a[href*="' + BRAINURL + '"]')[i].innerHTML = "Apertura cuenta de servicios";
    //i++;
  //});
  // Modifica el enlace "Apertura cuenta de servicios" si el cliente potencial no esta autorizado
      $('td[data-attribute$="dyn_consentimientoaccesodatos"][data-value="false"]').next('td').children().text("-").removeAttr("href");
});
});

function isEmptyOrNull(str){
 return (str === null || str === undefined || !str || 0 === str.length);
}
