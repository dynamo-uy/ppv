var languageSet = {
  "es-ES": {
    palabraClave: 'Documentación requerida',
    solicitud: 'Por favor adjuntar la siguiente información: (1) Copia de los Estatutos Sociales o Contrato Social; (2) Copia de los Registros Accionistas/Directores (en caso de aplicar); (3)Copia de Certificado de Vigencia (ultimo emitido, en caso de aplicar); (4) Documento que acredite la representación legal del Representante (en caso de aplicar); (5) Copia de los títulos accionarios (de corresponder); (6) Copia del certificado de incorporación (de corresponder); (7) Copia de las resoluciones de la sociedad (de corresponder), incluyendo la designación de los primeros directores; (8) Carta de referencias bancarias y profesionales y/o comerciales (vigencia no superior a 3 meses); (9) Documentos que acrediten el cumplimiento de las obligaciones tributarias con la administración fiscal que corresponda; (10) Prueba de Origen de Fondos, según declarado anteriormente.',
    direccion: 'Dirección',
    nextButtonEnviar: 'Enviar',
    nextButtonGuardar: 'Guardar',
    errorMessage: "Su formulario no puede ser enviado porque no cumple con alguna de las siguientes condiciones: <br/> ",
    condicion1: "- Haber cargado como mínimo 1 Accionista, 1 Director (si la cuenta es de Panama 3) y 1 Beneficiario Final (si la sociedad posee al menos 1 integrante de tipo persona física). <br/>",
    condicion2: "- La suma de los porcentajes de participación de los accionistas cargados debe ser igual a 100 (cien).",
  },
  "en-US": {
    palabraClave: 'Requested Documentation',
    solicitud: 'Please upload a copy of the following documentation, if applicable: (1) Company´s bylaws, (2) Share Register and Register of Directors, (3) Certificate of Goodstanding and Incumbency (last ones issued)  (4) Any document proving the legal powers of the Company´s representative, (5) Share certificates, (6) Certificate of incorporation, (7) Company´s resolutions, including the appointment of first directors, 8) Banking and Professional or Commercial references with a validity of no longer than three (3) months, 9) Tax returns in the country of residence of the shareholders and 10) Income source proof.',
    direccion: 'Address',
    nextButtonEnviar: 'Send',
    nextButtonGuardar: 'Save',
    errorMessage: "Your form can not be sent because it does not meet any of the following conditions: <br/> ",
    condicion1: "- Have loaded at least 1 Shareholder, 1 Director (if the account is from Panama 3) and 1 Final Beneficiary (if the company has at least 1 member of type natural entity). <br/>",
    condicion2: "- The sum of the participation percentages of the loaded shareholders must be equal to 100 (one hundred).",
  }
};
var language = languageSet[$('html').attr('lang')];

$(document).ready(function() {
  $("#dyn_formularioenviado").change(ChangeLabelButton);
  SolicitaAdjuntos();
  OcultarCampos();
});

function OcultarCampos() {
    let campoBusinessLine = $(`fieldset:contains("Business line")`);
    if (campoBusinessLine.length) {
      campoBusinessLine.hide();
    }                                   
    let campoBusinessLineSP = $(`fieldset:contains("Area de Servicios")`);
    if (campoBusinessLineSP.length) {
      campoBusinessLineSP.hide();
    } 
    $('h2[class="tab-title"]').hide();
}

function SolicitaAdjuntos() {
  $('#address1_composite_line1').prev('label')[0].innerText = language.direccion;
  $('#address1_composite_line2').prev('label').hide();
  $('#address1_composite_line3').prev('label').hide();
  $("#address1_composite_line2").hide();
  $("#address1_composite_line3").hide();
  
  $('label').each(function() {
    if ($(this).text() === language.palabraClave) {
      $(this).text(language.solicitud);
    }
  });
}

function ChangeLabelButton() {
	var fld = $("#dyn_formularioenviado_1").is(":checked");
	if(fld !== undefined && fld === true )
		$("#NextButton").attr('value', language.nextButtonEnviar);
	else
		$("#NextButton").attr('value', language.nextButtonGuardar);
}

$(window).load(function() {
  if ($('#dyn_origen').length) {
    $('#dyn_origen').parent().parent().hide();
  }
});
 
$('.text-danger').html('<span class="fa fa-exclamation-triangle" aria-hidden="true"></span>&nbsp'+language.errorMessage + language.condicion1 + language.condicion2);