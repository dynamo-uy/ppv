const BRAINURL = location.origin;
const languageSet = {
	      es: {
	          openServicesAccount: 'Apertura cuenta de servicios'
	      },
	      en: {
	          openServicesAccount: 'Open Services Account'
	      }
 };
var language;
$(document).ready(function(){
 	language = languageSet[ $('html').attr('lang').split('-')[0] ];
  	formatearLink();
 })

 $(".entitylist.entity-grid").on("loaded", function(){
	formatearLink(); 
 });

function formatearLink(){
    if( $(".entitylist.entity-grid")){    
 		var i=0;
 			$('td > a[href*="' + BRAINURL +'/Apertura"]').each(function(){
 				$('a[href*="' + BRAINURL +'/Apertura"]')[i].innerHTML = language.openServicesAccount;
 			  i++;
 			});
    }
} 
