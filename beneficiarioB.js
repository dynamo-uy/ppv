const BRAINURL = location.origin;
var languageSet = {
    "es-ES": {
			personaFisica: "Persona física",
			personaJuridica: "Persona jurídica",
			erroresTitle: 'Errores:',
			nombreAlternoVacio: 'El nombre alterno es obligatorio',
			apellidoVacio: 'El apellido es obligatorio',
			correoElectronicoVacio: 'El correo electrónico es obligatorio',
			correoElectronicoInvalido: 'El correo electrónico no es válido',
			nombreSociedadVacio: 'El nombre de la sociedad es obligatorio',
			nombreBeneficiarioFinalVacio: 'El nombre del beneficiario final es obligatorio',
			apellidoBeneficiarioFinalVacio: 'El apellido del benficiario final es obligatorio',
			correoElectronicoBeneficiarioFinalVacio: 'El correo electrónico del beneficiario final es obligatorio',
			correoElectronicoBeneficiarioFinalInvalido: 'El correo electrónico del beneficiario final no es válido',
			naturalezaIntegranteVacia: 'Debe indicar la naturaleza del integrante',
			beneficiarioFinal: 'Beneficiario Final',
    },
    "en-US": {
			personaFisica: "Individual",
			personaJuridica: "Legal person",
			erroresTitle: 'Errors:',
			nombreAlternoVacio: 'Name is mandatory',
			apellidoVacio: 'Surname is mandatory',
			correoElectronicoVacio: 'The email address is mandatory',
			correoElectronicoInvalido: 'The email address is not valid',
			nombreSociedadVacio: 'The entity\'s name is mandatory',
			nombreBeneficiarioFinalVacio: 'The final beneficiary\'s name is mandatory',
			apellidoBeneficiarioFinalVacio: 'The final beneficiary\'s surname is mandatory',
			correoElectronicoBeneficiarioFinalVacio: 'The final beneficiary\'s email address is mandatory',
			correoElectronicoBeneficiarioFinalInvalido: 'The final beneficiary\'s email address is not valid',
			naturalezaIntegranteVacia: 'The member\'s nature is mandatory',
			beneficiarioFinal: 'Final Beneficiary',
    }
};

var language  = languageSet[$('html').attr('lang')]; 

$(document).ready(function() {
	$('#dyn_director_0').on('click', function() {
			$('#dyn_cargo').val('');
	});
	$('#dyn_accionista_0').on('click', function() {
			$('#dyn_participacion').val('');
	});
	$('#dyn_apoderado_0').on('click', function() {
			$('#dyn_facultades').val('');
	});
	$('#dyn_jointtenancy_0').on('click', function() {
			$('#dyn_participacionjointtenancy').val('');
	});
	
  $('#InsertButton').on('mousedown', function(event) {
	
  	var errors = false;
	
		if($(this).attr('onclick')) {
			window.script = $(this).attr('onclick').split(':')[1];
			$(this).removeAttr('onclick');
		}
		
		var errorList = document.createElement('ul'),
			errorTitle = document.createElement('h4');
			
		errorTitle.setAttribute('class', 'alert-heading');
		errorTitle.innerText = language.erroresTitle;
		errorList.appendChild(errorTitle);
		switch($('#dyn_personalidad option:selected').text()) {
			case language.personaFisica:
				if($('#dyn_nombrealterno').val().trim().length < 1) {
					var li = document.createElement('li');
					li.innerText = language.nombreAlternoVacio;
					errorList.appendChild(li);
					errors = true;
				}
				if($('#dyn_apellido').val().trim().length < 1) {
					var li = document.createElement('li');
					li.innerText = language.apellidoVacio;
					errorList.appendChild(li);
					errors = true;
				}
				if($('#dyn_correoelectronico').val().trim().length < 1) {
					var li = document.createElement('li');
					li.innerText = language.correoElectronicoVacio;
					errorList.appendChild(li);
					errors = true;
				} else if(!validateEmail($('#dyn_correoelectronico').val())) {
					var li = document.createElement('li');
					li.innerText = language.correoElectronicoInvalido;
					errorList.appendChild(li);
					errors = true;
				}
				break;
			case language.personaJuridica:
				if($('#dyn_nombresociedad').val().trim().length < 1) {
					var li = document.createElement('li');
					li.innerText = language.nombreSociedadVacio;
					errorList.appendChild(li);
					errors = true;
				}
				if($('#dyn_beneficiariofinalnombre').val().trim().length < 1) {
					var li = document.createElement('li');
					li.innerText = language.nombreBeneficiarioFinalVacio;
					errorList.appendChild(li);
					errors = true;
				}
				if($('#dyn_beneficiariofinalapellido').val().trim().length < 1) {
					var li = document.createElement('li');
					li.innerText = language.apellidoBeneficiarioFinalVacio;
					errorList.appendChild(li);
					errors = true;
				}
				if($('#dyn_beneficiariofinalmail').val().trim().length < 1) {
					var li = document.createElement('li');
					li.innerText = language.correoElectronicoBeneficiarioFinalVacio;
					errorList.appendChild(li);
					errors = true;
				} else if(!validateEmail($('#dyn_beneficiariofinalmail').val())) {
					var li = document.createElement('li');
					li.innerText = language.correoElectronicoBeneficiarioFinalInvalido;
					errorList.appendChild(li);
					errors = true;
				}
				break;
			default:
				var li = document.createElement('li');
					li.innerText = language.naturalezaIntegranteVacia;
					errorList.appendChild(li);
				break;
		}
		if(errors) {
			$('.alert').css('display', 'none');
			var alert = document.createElement('div');
			alert.setAttribute('class', 'alert alert-danger');
			alert.setAttribute('role', 'alert');
			alert.appendChild(errorList);
			document.getElementById('content_form').prepend(alert);
			window.location.href = '#content_form';
		} else {
			$(this).attr('onclick', window.script);
		}
	});

	function validateEmail(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
	}

  $.getScript(BRAINURL + '/dynamo-cdn', function() {
    console.log('Extensiones de Dynamo cargadas correctamente');
  });
  
  SetFieldsVisibility();
  $("#dyn_director").change(SetFieldsVisibility);
  $("#dyn_accionista").change(SetFieldsVisibility);
  $("#dyn_apoderado").change(SetFieldsVisibility);
  $("#dyn_jointtenancy").change(SetFieldsVisibility);  
	$("#dyn_personalidad").change(SetFieldsVisibility);

});

function SetFieldsVisibility() {
	ShowOrHide("#dyn_director_1", "#dyn_cargo");
	ShowOrHide("#dyn_accionista_1", "#dyn_participacion");
	ShowOrHide("#dyn_apoderado_1", "#dyn_facultades");
	ShowOrHide("#dyn_jointtenancy_1", "#dyn_participacionjointtenancy");		
	ShowOrHideKind();	
}

function ShowOrHide(origen, destino){
	var fld = $(origen).is(":checked");
	if (fld != undefined && fld == true )
    $(destino).closest("tr").show();
  else
    $(destino).closest("tr").hide();
}

function ShowOrHideKind(){
  var valKind = $("#dyn_personalidad option:selected").text().toUpperCase();
	
	if(valKind === language.personaFisica.toUpperCase())
	{
	  $("#dyn_nombresociedad").closest("tr").hide();	
    $("#dyn_beneficiariofinal").closest("tr").show();
		$("#dyn_nombrealterno").closest("tr").show();
		$("#dyn_apellido").closest("tr").show();
		$("#dyn_correoelectronico").closest("tr").show();
	  $("#dyn_beneficiariofinalnombre").closest("tr").hide();
		$("#dyn_beneficiariofinalapellido").closest("tr").hide();
		$("#dyn_beneficiariofinalmail").closest("tr").hide();
		$(".tab-title")[3].innerText = "";
	}
	else
	{
	  $("#dyn_nombresociedad").closest("tr").show();
	  $("#dyn_beneficiariofinal").closest("tr").hide();
		$("#dyn_nombrealterno").closest("tr").hide();
		$("#dyn_apellido").closest("tr").hide();
		$("#dyn_correoelectronico").closest("tr").hide();
	  $("#dyn_beneficiariofinalnombre").closest("tr").show();
		$("#dyn_beneficiariofinalapellido").closest("tr").show();
		$("#dyn_beneficiariofinalmail").closest("tr").show();
		$(".tab-title")[3].innerText = language.beneficiarioFinal;
	}
}
