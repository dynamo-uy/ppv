$(document).ready(function() {
  const template = $(`
    <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="message" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="message">Enter Received code</h5>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="accountCode" class="col-form-label">Code:</label>
            <input type="text" class="form-control" id="accountCode" required>
            <div class="valid-feedback" id="codeMessage"></div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="submitCode">Validate</button>
      </div>
    </div>
  </div>
</div>
  `);
  const url = new URL(location.href);
  
  if (!url.searchParams.get('cuentaid')) {
    $('body').append(template);
    $('#messageModal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#submitCode').on('click', function(e) {
      const codigo = $('#accountCode').val().trim();
      
      if (codigo.length === 36) {
        redirectTo = url.protocol + '//' + url.host + url.pathname + '?cuentaid=' + codigo;
        $('#codeMessage').text('Processing...');
        window.location = redirectTo;
      } else {
        $('#codeMessage').text('The code is not valid');
      }
    });
  }
});