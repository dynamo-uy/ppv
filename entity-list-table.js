$(window).on('load', function() {
	// Carga el script de DataTables asincrónicamente
    $.getScript('https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.js', function() {
		// Añade los estilos de DataTable al documento
        $('<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/r-2.2.2/datatables.min.css"/>').appendTo('head');

		// Oculta campo de búsqueda predeterminado
        $('.view-toolbar').hide();

		// Obtiene de la tabla original las columnas y filas visibles
        let visibleHeaderColumns = $('table thead tr th:visible');
        let visibleRows = $('table tbody tr:visible');

		// Inserta las columnas y filas visisbles en las secciones de la tabla correspondientes
        let tableHead = $('<thead>').append( $('<tr>').append(visibleHeaderColumns) );
        let tableBody = $('<tbody>').append(visibleRows);
		
		// Añade una nueva tabla al div con clase view-grid e inserta las secciones agregadas
        $('.view-grid').html('').append('<table id="data-table" class="table table-striped">');
        $('#data-table').append(tableHead).append(tableBody);
		
        $('#data-table thead tr th a, #data-table tbody tr td a').css('text-decoration', 'underline'); // Aplica CSS para que se distingan los colores de las columnas ordenables por las que no así como los enlaces.
		$('#data-table thead tr th a:hidden').parent().hide(); //  Oculta las columas no visibles

        // Adaptación de Idiomas
        switch($('html').attr('lang')) {
            case 'es-ES':
                lang = '//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json';
                break;
            default:
                lang = '//cdn.datatables.net/plug-ins/1.10.19/i18n/English.json';
                break;
        }
        
		// Carga de DataTables
        $('#data-table').DataTable({
            lengthChange: false,
            ordering: false,
            language: {
                url: lang
            }
        });

        // DataTable Translations: https://datatables.net/plug-ins/i18n/#Translations 
    });
});