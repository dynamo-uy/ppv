document
    .querySelectorAll('.modal-dialog')
    .forEach(modal => {
        modal.style.width = '90%'
    });

var finicio = true;
var proveedor = getProveedor();
//
$(".entitylist.entity-grid").on("loaded", function () {
	  filtrarGrilla();
});
//
$(document).ready(function() {
	filtrarGrilla();
})
//
function filtrarGrilla(){
	
	  if( $(".entitylist.entity-grid")){
		  console.log("Grilla carga correctamente")
		  $(".entitylist.entity-grid").find("tr").each(function (){ 
		      if(finicio){
		         finicio = false;
		      }
		      else{
		        var eliminar = true;
		        var elemento = $(this);
		        var itemProveedor = elemento.find("td[data-attribute='dyn_proveedorasociadocuenta']").data("value") !== undefined ? elemento.find("td[data-attribute='dyn_proveedorasociadocuenta']").data("value").Name : undefined; 
		        
		        if(itemProveedor === undefined){
					eliminar = true;
				}
		        else if(proveedor !== undefined && proveedor !== null && proveedor === itemProveedor){
		          eliminar = false;
				}
			    if(eliminar === true){
				 $(this).closest("tr[data-entity='account']").hide(); 
				}

		      }
		    });        
		  $(".entitylist.entity-grid").find("td[data-attribute='dyn_proveedorasociadocuenta']").hide();
		  $(".entitylist.entity-grid").find("a[aria-label='Proveedor Asociado Cuenta']").hide();
	  }else {
		  console.log("Grilla no Carga")
	  }
  };
